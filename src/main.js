/* main.js
 *
 * Copyright 2021 url
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

pkg.initGettext();
pkg.initFormat();
pkg.require({
	'Gio': '2.0',
	'Gtk': '3.0'
});

const { Gio, Gtk } = imports.gi;
const { CalculadoradefechasWindow } = imports.window;

const sMinutes = 60, sHours = sMinutes * 60, sDays = sHours * 24,
	sWeek = sDays * 7, sMonth = sDays * 30, sYear = sDays * 365;

function nuPrefix(num, len = 2, pre = '0') {
	let contentLen = `${num}`.length;

	if (contentLen < len)
		return `${pre.repeat(len - contentLen)}${num}`;

	return `${num}`;
}

function dateChanged(cal, label, time) {
	let [anio, mes, dia] = cal.get_date(), hora = time.hrs.get_value(),
		minuto = time.min.get_value(), segundo = time.sec.get_value(), date;

	date = `Fecha:\t${nuPrefix(dia)}/${nuPrefix(mes + 1)}/${anio}\tHora:\t`;
	date += `${nuPrefix(hora)}:${nuPrefix(minuto)}:${nuPrefix(segundo)}`;

	label.set_text(date);
}

function internalsWin(win) {
	const btnDone = win._btn_done,
		btnClean = win._btn_clean,
		calendar = win._calendar,
		hours = win._hrs,
		minutes = win._mins,
		seconds = win._secs,
		txtDate = win._txt_date,
		calendarStart = win._calendar_start,
		hoursStart = win._hrs_start,
		minutesStart = win._mins_start,
		secondsStart = win._secs_start,
		txtDateStart = win._txt_date_start,
		calendarEnd = win._calendar_end,
		hoursEnd = win._hrs_end,
		minutesEnd = win._mins_end,
		secondsEnd = win._secs_end,
		txtDateEnd = win._txt_date_end,
		change = win._spin_change, type = win._com_type,
		operation = win._com_opera, output = win._txtout;

	let date = new Date(), saveTime, saveTimeStart, saveTimeEnd;
	// Asignar fecha actual a los componentes de fecha
	calendar.select_month(date.getMonth(), date.getFullYear());
	minutes.set_value(date.getMinutes());
	calendar.select_day(date.getDate());
	hours.set_value(date.getHours());

	calendarStart.select_month(date.getMonth(), date.getFullYear());
	minutesStart.set_value(date.getMinutes());
	calendarStart.select_day(date.getDate());
	hoursStart.set_value(date.getHours());

	calendarEnd.select_month(date.getMonth(), date.getFullYear());
	minutesEnd.set_value(date.getMinutes());
	calendarEnd.select_day(date.getDate());
	hoursEnd.set_value(date.getHours());
	// Los label tienen que tener un texto por defecto, se les asigna la fecha actual
	dateChanged(calendarStart, txtDateStart, { hrs: hoursStart, min: minutesStart, sec: secondsStart });
	dateChanged(calendarEnd, txtDateEnd, { hrs: hoursEnd, min: minutesEnd, sec: secondsEnd });
	dateChanged(calendar, txtDate, { hrs: hours, min: minutes, sec: seconds });

	/* Calculadora */
	calendar.connect('day-selected', c =>
		dateChanged(c, txtDate, { hrs: hours, min: minutes, sec: seconds }));

	hours.connect('value-changed', h =>
		dateChanged(calendar, txtDate, { hrs: h, min: minutes, sec: seconds }));

	minutes.connect('value-changed', m =>
		dateChanged(calendar, txtDate, { hrs: hours, min: m, sec: seconds }));

	seconds.connect('value-changed', s =>
		dateChanged(calendar, txtDate, { hrs: hours, min: minutes, sec: s }));

	/* Diferencia */
	// Start
	calendarStart.connect('day-selected', c =>
		dateChanged(c, txtDateStart, { hrs: hoursStart, min: minutesStart, sec: secondsStart }));

	hoursStart.connect('value-changed', h =>
		dateChanged(calendarStart, txtDateStart, { hrs: h, min: minutesStart, sec: secondsStart }));

	minutesStart.connect('value-changed', m =>
		dateChanged(calendarStart, txtDateStart, { hrs: hoursStart, min: m, sec: secondsStart }));

	secondsStart.connect('value-changed', s =>
		dateChanged(calendarStart, txtDateStart, { hrs: hoursStart, min: minutesStart, sec: s }));
	// End
	calendarEnd.connect('day-selected', c =>
		dateChanged(c, txtDateEnd, { hrs: hoursEnd, min: minutesEnd, sec: secondsEnd }));

	hoursEnd.connect('value-changed', h =>
		dateChanged(calendarEnd, txtDateEnd, { hrs: h, min: minutesEnd, sec: secondsEnd }));

	minutesEnd.connect('value-changed', m =>
		dateChanged(calendarEnd, txtDateEnd, { hrs: hoursEnd, min: m, sec: secondsEnd }));

	secondsEnd.connect('value-changed', s =>
		dateChanged(calendarEnd, txtDateEnd, { hrs: hoursEnd, min: minutesEnd, sec: s }));

	btnClean.connect('clicked', b => output.text = "");

	btnDone.connect('clicked', b => {
		switch (win._stacks.get_stack().get_visible_child().name) {
			case "Calc":
				let [anio, mes, dia] = calendar.get_date(), initDate, lastDate,
					milsChange, valChange = change.get_value(), txtmp = '';

				initDate = new Date(
					anio, mes, dia,
					hours.get_value(),
					minutes.get_value(),
					seconds.get_value()
				);

				switch (type.get_active_text()) {
					case 'Hora(s)':
						milsChange = valChange * (sHours * 1000);
						break;
					case 'Minuto(s)':
						milsChange = valChange * (sMinutes * 1000);
						break;
					case 'Segundo(s)':
						milsChange = valChange * 1000;
						break;
					case 'Año(s)':
						milsChange = valChange * (sYear * 1000);
						break;
					case 'Mes(es)':
						milsChange = valChange * (sMonth * 1000);
						break;
					case 'Semana(s)':
						milsChange = valChange * (sWeek * 1000);
						break;
					case 'Día(s)':
						milsChange = valChange * (sDays * 1000);
						break;
				}

				if (operation.get_active_text() === '+')
					lastDate = new Date(initDate.getTime() + milsChange);
				else
					lastDate = new Date(initDate.getTime() - milsChange);
				// TODO: contemplar cuando hay una fecha fuera del tiempo UNIX
				//	if (! lastDate instanceof Date || isNaN(lastDate))
				//		Dialogo de error;
				if (output.text !== "")
					txtmp += `${output.text}\n`;

				txtmp += `${txtDate.get_text().replace(/\t/g, ' ')}\t`;
				txtmp += `${operation.get_active_text()}\t`;
				txtmp += `${valChange} ${type.get_active_text()}\t=\t`;
				txtmp += `Fecha: ${nuPrefix(lastDate.getDate())}/`;
				txtmp += `${nuPrefix(lastDate.getMonth() + 1)}/`;
				txtmp += `${nuPrefix(lastDate.getFullYear())} Hora: `;
				txtmp += `${nuPrefix(lastDate.getHours())}:`;
				txtmp += `${nuPrefix(lastDate.getMinutes())}:`;
				txtmp += `${nuPrefix(lastDate.getSeconds())}`;

				let oldOuts = output.text.split("\n");
				let newOuts = txtmp.split("\n");
				// Si la ultima fila es igual al resultado nuevo no concatenar
				if (oldOuts[oldOuts.length - 1] !== newOuts[newOuts.length - 1])
					output.text = txtmp;
				break;
			case "Diff":
				let [anioStart, mesStart, diaStart] = calendarStart.get_date(),
					[anioEnd, mesEnd, diaEnd] = calendarEnd.get_date(),
					startDate, endDate, diffSecs, anios = 0, meses = 0,
					semanas = 0, dias = 0, horas = 0, minutos = 0, segundos = 0,
					txtemp = "";

				startDate = new Date(
					anioStart, mesStart, diaStart,
					hoursStart.get_value(),
					minutesStart.get_value(),
					secondsStart.get_value()
				);

				endDate = new Date(
					anioEnd, mesEnd, diaEnd,
					hoursEnd.get_value(),
					minutesEnd.get_value(),
					secondsEnd.get_value()
				);

				if (output.text !== "")
					txtemp += `${output.text}\n`;

				if (startDate > endDate) {
					let tmp;
					tmp = startDate;
					startDate = endDate;
					endDate = tmp;

					txtemp += `${txtDateEnd.get_text().replace(/\t/g, ' ')}\t−\t`;
					txtemp += `${txtDateStart.get_text().replace(/\t/g, ' ')}\t=\t`;
				} else {
					txtemp += `${txtDateStart.get_text().replace(/\t/g, ' ')}\t−\t`;
					txtemp += `${txtDateEnd.get_text().replace(/\t/g, ' ')}\t=\t`;
				}

				diffSecs = (endDate - startDate) / 1000;

				anios = parseInt(diffSecs / sYear);
				diffSecs -= anios * sYear;

				meses = parseInt(diffSecs / sMonth);
				diffSecs -= meses * sMonth;

				semanas = parseInt(diffSecs / sWeek);
				diffSecs -= semanas * sWeek;

				dias = parseInt(diffSecs / sDays);
				diffSecs -= dias * sDays;

				horas = parseInt(diffSecs / sHours);
				diffSecs -= horas * sHours;

				minutos = parseInt(diffSecs / sMinutes);
				diffSecs -= minutos * sMinutes;

				segundos = diffSecs;

				if (anios !== 0) txtemp += `${anios} año(s), `;
				if (meses !== 0) txtemp += `${meses} mes(es), `;
				if (semanas !== 0) txtemp += `${semanas} semana(s), `;
				if (dias !== 0) txtemp += `${dias} día(s), `;
				if (horas !== 0) txtemp += `${dias} hora(s), `;
				if (minutos !== 0) txtemp += `${minutos} minuto(s) `;

				txtemp += `${segundos} segundo(s)`;

				let oldOut = output.text.split("\n");
				let newOut = txtemp.split("\n");
				// Si la ultima fila es igual al resultado nuevo no concatenar
				if (oldOut[oldOut.length - 1] !== newOut[newOut.length - 1])
					output.text = txtemp;

				break;
		}
	});
}

function main(argv) {
	const application = new Gtk.Application({
		application_id: 'url.calc.dates',
		flags: Gio.ApplicationFlags.FLAGS_NONE,
	});

	application.connect('activate', app => {
		let win = app.win;

		if (!win)
			win = new CalculadoradefechasWindow(app);

		win.connect('show', internalsWin);
		win.present();
	});

	return application.run(argv);
}
