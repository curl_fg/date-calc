/* window.js
 *
 * Copyright 2021 url
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gtk } = imports.gi;

var CalculadoradefechasWindow = GObject.registerClass({
	GTypeName: 'CalculadoradefechasWindow',
	Template: 'resource:///url/calc/dates/window.ui',
	InternalChildren: [
		'btn_cal_end',
		'btn_cal_start',
		'btn_cal',
		'btn_clean',
		'btn_done',
		'calendar',
		'com_opera',
		'com_type',
		'hrs',
		'mins',
		'pop_date',
		'secs',
		'spin_change',
		'stacks',
		'txt_date',
		'txtout',
		"calendar_end",
		"calendar_start",
		"hrs_end",
		"hrs_start",
		"mins_end",
		"mins_start",
		"secs_end",
		"secs_start",
		"txt_date_end",
		"txt_date_start"
	]
}, class CalculadoradefechasWindow extends Gtk.ApplicationWindow {
	_init(application) {
		super._init({ application });
	}
});

